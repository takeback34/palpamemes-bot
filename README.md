# PalpaMemes Bot

The PalpaMemes Bot is a discord bot. Its main use is to save the memes posted on the PalpAlliance server.

## Installation

```bash
npm install
```

## Scripts

- `npm start`: Launch the bot
- `npm lint`: Run the linter on the main file
- `npm lint:fix`: Run the linter on the main file with quick fix enabled