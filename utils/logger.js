const winston = require('winston');

const format = winston.format.printf(({ level, message, timestamp }) => {
	return `[${timestamp}] (${level.toUpperCase()}): ${message}`;
});

const Logger = winston.createLogger({
	format: winston.format.combine(
		winston.format.timestamp(),
		format,
	),
	transports: [
		new winston.transports.Console(),
		new winston.transports.File({ filename: 'bot.log' }),
	],
});

module.exports = Logger;