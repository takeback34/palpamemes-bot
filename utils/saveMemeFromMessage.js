const fs = require('fs');
const fetch = require('node-fetch');
const { v4: generateUuid } = require('uuid');
const Logger = require('./logger.js');
const config = require('../config.json');

const { memesFolder, imageFormats, emoji, argsThreshold } = config;

/**
 * Represents an HTTP Response Error
 */
class HTTPResponseError extends Error {
	constructor(response, ...args) {
		super(`HTTP Response Error ${response.status}: ${response.statusText}`, ...args);
		this.response = response;
	}
}

/**
 * "Safe" fetch from an url
 * @param {string} url The url to fetch from
 * @returns {any}
 */
const fetchAndCheckStatus = async url => {
	const response = await fetch(url);
	if(!response.ok) {
		throw new HTTPResponseError(response);
	}
	return response;
};

/**
 * Fetches data and saves it locally
 * @param {string} url The url of the data
 * @param {string} fileName The name of the saved file
 * @param {string} format The format of the saved file
 * @param {string} dir The directory to save the file to (Current directory by default)
 * @returns {Promise}
 * @throws Throws an error if one of the operation fails
 */
const fetchAndSave = async (url, fileName, format, dir = '.') => {
	const response = await fetchAndCheckStatus(url);
	const buffer = await response.buffer();
	return fs.promises.writeFile(`${dir}/${fileName}.${format}`, buffer);
};

/**
 * Retrieves the format of the data whose url is specified
 * @param {string} url The url of the data (Here, a discord attachement url)
 * @returns {string}
 */
const getFormatFromURL = url => {
	const splitedUrl = url.split('.');
	return splitedUrl[splitedUrl.length - 1];
};

/**
 * Save the memes contained in a message.
 * Here, a meme has to be both an attachment and a picture.
 * @param {Message} message The message that contains the memes to be saved
 * @returns undefined
 */
const saveMemeFromMessage = message => {
	if(!message.attachments.size) {
		Logger.info('No attachements found');
		return;
	}
	else if(message.attachments.size > argsThreshold) {
		message.channel.send(`Too many attachments. Maximum is ${argsThreshold}.`);
	}
	message.attachments.forEach(attachment => {
		const format = getFormatFromURL(attachment.url);
		const uuid = generateUuid();
		if(!imageFormats.includes(format.toLowerCase())) {
			Logger.info('Attachement is not a picture');
			return;
		}
		fetchAndSave(attachment.url, uuid, format, memesFolder.pictures)
			.then(() => fs.promises.writeFile(`${memesFolder.infos}/${uuid}.json`, JSON.stringify({
				uuid,
				author: message.author.id,
				savedAt: message.createdAt.toJSON(),
				message: message.id,
				format,
			})))
			.then(() => {
				Logger.info('Meme saved successfully');
				message.react(emoji.success);
			})
			.catch(error => {
				Logger.error(`An error occurred while saving the meme: ${error.message}`);
				message.react(emoji.failure);
			});
	});
};

module.exports = saveMemeFromMessage;