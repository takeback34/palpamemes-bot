const config = require('../config.json');
const Logger = require('../utils/logger.js');

const { memesChannel } = config;

const execute = (client) => {
	Logger.info('Bot ready !');
	client.channels.fetch(memesChannel.id)
		.then(channel => {
			Logger.info(`${channel.name} channel retrieved`);
			client.memesChannel = channel;
		})
		.catch(error => {
			Logger.error(`Could not retrieve memes channel: ${error.message}`);
		});
};

module.exports = {
	name: 'ready',
	once: true,
	execute,
};