const config = require('../config.json');
const Logger = require('../utils/logger.js');
const saveMemeFromMessage = require('../utils/saveMemeFromMessage.js');

const { prefix, argsThreshold, memesChannel } = config;

const execute = (message, client) => {
	if(message.author.bot) return;

	if(message.content.startsWith(prefix)) {
		const args = message.content.slice(prefix.length).trim().split(/ +/);
		const commandName = args.shift().toLowerCase();

		if(!client.commands.has(commandName)) return;

		Logger.info(
			`${message.author.username}<${message.author.id}>: ${commandName} command with ${
				args.length ? `args ${args.join(', ')
				}` : 'no args'} in ${
				message.channel.type === 'dm' ? 'DMs' : `the channel ${message.channel.name}`
			}`,
		);

		if(args.length > argsThreshold) {
			message.channel.send(`Too many arguments. Maximum is ${argsThreshold}.`);
			return;
		}

		const command = client.commands.get(commandName);
		if(command.args && !args.length) {
			message.channel.send(`The command \`${command.name}\` requires arguments. Type \`${prefix}help ${command.name}\` to get information on this command.`);
		}
		try {
			command.execute(message, args);
		}
		catch (error) {
			Logger.error(`There was an error while executing the last command: ${error.message}`);
			message.channel.send('An error occurred, sorry ! :(');
		}
	}

	if(message.channel.id === memesChannel.id) {
		Logger.info(`${message.author.username}<${message.author.id}>: in ${message.channel.name} channel`);
		saveMemeFromMessage(message);
	}
};

module.exports = {
	name: 'message',
	execute,
};