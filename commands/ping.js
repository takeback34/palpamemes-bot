const execute = (message) => {
	message.channel.send('Pong !');
};

module.exports = {
	name: 'ping',
	description: 'Sends "Pong !".',
	execute,
};