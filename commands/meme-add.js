const config = require('../config.json');
const Logger = require('../utils/logger.js');
const saveMemeFromMessage = require('../utils/saveMemeFromMessage.js');

const { memesChannel } = config;

const execute = (message, args) => {
	args.forEach(arg => {
		message.client.memesChannel.messages.fetch(arg)
			.then(msg => {
				saveMemeFromMessage(msg);
			})
			.catch(error => {
				Logger.error(`Could not retrieve message in channel ${memesChannel.id}: ${error}`);
				message.channel.send(`No message with ID ${arg} in channel ${memesChannel.name}`);
			});
	});
};

module.exports = {
	name: 'meme-add',
	description: 'Saves the memes contained in the message(s) whose IDs are given as arguments.',
	args: true,
	usage: '<message_id1> <message_id2> ...',
	execute,
};