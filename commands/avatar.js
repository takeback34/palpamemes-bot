const execute = (message) => {
	if(!message.mentions.users.size) {
		message.channel.send('Here\'s your avatar: ', { files: [message.author.displayAvatarURL({ format:'png', dynamic:true })] });
		return;
	}
	message.mentions.users.forEach(user => {
		message.channel.send(`Here's the avatar of ${user.username}: `, { files:[user.displayAvatarURL({ format:'png', dynamic:true })] });
	});
};

module.exports = {
	name: 'avatar',
	description: 'Shows the avatar of the users mentionned if there are any. Otherwise, returns the avatar of the command\'s author.',
	usage: '@<user1> @<user2> ...',
	execute,
};