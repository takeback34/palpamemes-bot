const fs = require('fs');
const config = require('../config.json');
const Logger = require('../utils/logger.js');

const { memesFolder, emoji } = config;

const execute = (message) => {
	fs.promises.readdir(memesFolder.infos)
		.then(files => {
			const jsonFiles = files.filter(filename => filename.endsWith('.json'));
			const selectedJson = jsonFiles[Math.floor(Math.random() * jsonFiles.length)];
			return fs.promises.readFile(`${memesFolder.infos}/${selectedJson}`)
				.then(result => JSON.parse(result));
		})
		.then(infos => {
			return fs.promises.readFile(`${memesFolder.pictures}/${infos.uuid}.${infos.format}`);
		})
		.then(image => {
			message.channel.send('', { files: [image] });
		})
		.catch(error => {
			Logger.error(`Could not retrieve meme: ${error.message}`);
			message.react(emoji.failure);
		});
};

module.exports = {
	name: 'meme',
	description: 'Shows a random meme among those which were previously saved.',
	execute,
};