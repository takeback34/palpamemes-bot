const config = require('../config.json');

const { prefix } = config;

const execute = (message, args) => {
	const reply = [];
	const { commands } = message.client;

	if(!args.length) {
		reply.push('Here are all the commands available:');
		reply.push(commands.map(command => `- \`${command.name}\`: ${command.description}`).join('\n'));
		reply.push(`\nType \`${prefix}help <command>\` to get more information about a command.`);

		message.channel.send(reply);
		return;
	}

	const name = args[0].toLowerCase();
	if(!commands.has(name)) {
		message.channel.send(`The \`${prefix}${name}\` command does not exist.`);
		return;
	}
	const command = commands.get(name);

	reply.push(`**Name:** ${command.name}`);
	if(command.description) reply.push(`**Description**: ${command.description}`);
	reply.push(`**Usage:** \`${prefix}${command.name}${command.usage ? ` ${command.usage}` : ''}\``);
	message.channel.send(reply);
};

module.exports = {
	name: 'help',
	description: 'Lists all the bot\'s available commands or shows info about a specific command.',
	usage: '<command>',
	execute,
};